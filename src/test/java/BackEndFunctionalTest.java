
import BackEndFunctional.Models.Root;
import BackEndFunctional.Models.Shipment;
import BackEndFunctional.Utility.TestBase;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

/**
 * Backend Test
 */
public class BackEndFunctionalTest extends TestBase {

    @Test(description = "Validating if at least one shipment available for queried vehicleType")
    public void validateAtLeastOneShipmentWithVehicleType(){
          Response response = given()
                  .header("Content-Type", ContentType.JSON)
                  .get("/entries");
        List<Object> entries = response.jsonPath().getList("entries");
        Assert.assertEquals(1425, entries.size());
    }

    @Test(dataProvider = "backendData", description = "Validating if at least one shipment available at queried coordinates", enabled = false)
    public void validateIfLocationIsHavingShipments(int lon, int lat){
        Root response = given()
                .headers(headersHelper())
                .and()
                .queryParams(queryCoordinatesParamsHelper(lon,lat))
                .get("/marketplace")
                .as(Root.class);

        Assert.assertTrue(response.getShipments().size()!=0);
    }


}
